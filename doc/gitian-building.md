Gitian building
================

This file was moved to [the bitcoinclassic Core documentation repository](https://github.com/bitcoinclassic-core/docs/blob/master/gitian-building.md) at [https://github.com/bitcoinclassic-core/docs](https://github.com/bitcoinclassic-core/docs).
