// Copyright (c) 2011-2014 The Bitcoin Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef BITCOINCLASSIC_QT_BITCOINCLASSICADDRESSVALIDATOR_H
#define BITCOINCLASSIC_QT_BITCOINCLASSICADDRESSVALIDATOR_H

#include <QValidator>

/** Base58 entry widget validator, checks for valid characters and
 * removes some whitespace.
 */
class bitcoinclassicAddressEntryValidator : public QValidator
{
    Q_OBJECT

public:
    explicit bitcoinclassicAddressEntryValidator(QObject *parent);

    State validate(QString &input, int &pos) const;
};

/** bitcoinclassic address widget validator, checks for a valid bitcoinclassic address.
 */
class bitcoinclassicAddressCheckValidator : public QValidator
{
    Q_OBJECT

public:
    explicit bitcoinclassicAddressCheckValidator(QObject *parent);

    State validate(QString &input, int &pos) const;
};

#endif // BITCOINCLASSIC_QT_BITCOINCLASSICADDRESSVALIDATOR_H
