#!/usr/bin/env bash

export LC_ALL=C
TOPDIR=${TOPDIR:-$(git rev-parse --show-toplevel)}
BUILDDIR=${BUILDDIR:-$TOPDIR}

BINDIR=${BINDIR:-$BUILDDIR/src}
MANDIR=${MANDIR:-$TOPDIR/doc/man}

BITCOINCLASSICD=${BITCOINCLASSICD:-$BINDIR/bitcoinclassicd}
BITCOINCLASSICCLI=${BITCOINCLASSICCLI:-$BINDIR/bitcoinclassic-cli}
BITCOINCLASSICTX=${BITCOINCLASSICTX:-$BINDIR/bitcoinclassic-tx}
BITCOINCLASSICQT=${BITCOINCLASSICQT:-$BINDIR/qt/bitcoinclassic-qt}

[ ! -x $BITCOINCLASSICD ] && echo "$BITCOINCLASSICD not found or not executable." && exit 1

# The autodetected version git tag can screw up manpage output a little bit
BXCVER=($($BITCOINCLASSICCLI --version | head -n1 | awk -F'[ -]' '{ print $6, $7 }'))

# Create a footer file with copyright content.
# This gets autodetected fine for bitcoinclassicd if --version-string is not set,
# but has different outcomes for bitcoinclassic-qt and bitcoinclassic-cli.
echo "[COPYRIGHT]" > footer.h2m
$BITCOINCLASSICD --version | sed -n '1!p' >> footer.h2m

for cmd in $BITCOINCLASSICD $BITCOINCLASSICCLI $BITCOINCLASSICTX $BITCOINCLASSICQT; do
  cmdname="${cmd##*/}"
  help2man -N --version-string=${BXCVER[0]} --include=footer.h2m -o ${MANDIR}/${cmdname}.1 ${cmd}
  sed -i "s/\\\-${BXCVER[1]}//g" ${MANDIR}/${cmdname}.1
done

rm -f footer.h2m
